# frozen_string_literal: true

# simple module
module BasicMath
  def self.add(num1, num2)
    num1 + num2
  end

  def self.abs_add(num1, num2)
    add(num1, num2).abs
  end
end
