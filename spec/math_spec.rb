# frozen_string_literal: true

require 'rspec'

require_relative '../lib/math'

describe BasicMath do
  it 'should add two numbers correctly' do
    expect(BasicMath.add(1, 2)).to eq 3
    expect(BasicMath.add(1, -1)).to eq 0
  end

  it 'should add two numbers absolutely correctly' do
    expect(BasicMath.abs_add(-1, -10)).to eq 11
  end
end

describe BasicMath do
  where(:num1, :num2, :answer) do
    [
      [1, 2, 3],
      [5, 8, 13],
      [0, 0, 0],
      [-1, 1, 0],
      [-11, -100, -111]
    ]
  end

  with_them do
    it 'should add two numbers correctly' do
      expect(BasicMath.add(num1, num2)).to eq answer
    end
  end

  with_them do
    it 'should add two numbers absolutely correctly' do
      expect(BasicMath.abs_add(num1, num2)).to eq answer.abs
    end
  end
end
