
# cookiecutter_test

cookiecutter_test is a ruby project

## Installation

```bash
bundler install
```

## Development

```
bundler install --with development
```

## Testing

Install testing dependencies with:

```
bundler install --with test
```

### RSpec

This project uses [RSpec](https://rspec.info/) for unit tests. Tests may be
run with:

```bash
bin/rspec
```

### RuboCop

[RuboCop](https://github.com/rubocop-hq/rubocop) is used for linting the
ruby source of this project. Run rubocop against cookiecutter_test with:

```bash
bin/rubocop
```
